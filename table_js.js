{
	let table = document.getElementById('tbl')
	let tbody = document.getElementById('tbody')
	let tRow = table.rows
	let td = table.querySelectorAll('td')
	let delRow = document.getElementById('del-row')
	let delCell = document.getElementById('del-cell')
	let addRow = document.getElementById('add-row')
	let addCell = document.getElementById('add-cell')
	let btnDel = document.getElementsByClassName('del')
	let rowIndex
	let cellIndex
	let timer
	//Таймер на изчезновение кнопок удаления 
    let timeOut = () => timer = setTimeout(button.visibleBtn(false), 3000)

    //Класс удаления и добавления строк и столбцов
	class DelAddEl {
		//Удаление строк и столбцов
		deleteEl () {
			if (this === delRow) {
				table.deleteRow(rowIndex)
			} else if (this === delCell) {
				for (let i = 0; i <= tRow.length - 1; i++) {
					tRow[i].deleteCell(cellIndex)
				}
			}
			button.hideDel(this)
		}
		//Добавление строк и столбцов
		addE() {
			if (this === addRow) {
				let countCell = table.rows[0].cells.length
				let newRow = table.insertRow()
				newRow.classList.add('t-row')
				for (let i = 0; i <= countCell - 1; i++) {
					let newCell = newRow.insertCell()
					button.addNewCell(newCell)
				}
				button.hideDel(delRow)
			} else if (this === addCell) {
				for (let i = 0; i <= tRow.length - 1; i++){
					let newCell = tRow[i].insertCell()
					button.addNewCell(newCell)
				}
				button.hideDel(delCell)
			}
		}
	}

	//клас повидения кнопок
	class DisplayBtn {
		//Появление кнопок при навидении на таблицу
		visibleBtn(disp) {
			return function() {
				for (let i = 0; i <= btnDel.length - 1; i++) {
					btnDel[i].addEventListener('mouseleave', timeOut)
					//что бы кнопки удаления оставались активными если навести на них
					btnDel[i].addEventListener('mouseover', () => clearTimeout(timer))
					if (disp == true) {
						clearTimeout(timer)
						btnDel[i].classList.add('fadeIn','animated')
					} else if (disp == false){
						btnDel[i].classList.remove('fadeIn')
						btnDel[i].classList.add('fadeOut')
					}
				}
			}
		}
		//Когда столбец или строка остается одна
		//кнопки удаления скрываются соответстенно
		hideDel(delbtn) {
			for (var i = 0; i <= tRow.length - 1; i++) {
				if (delbtn === delRow && tRow.length === 1 || delbtn === delCell && tRow[i].cells.length === 1) {
					delbtn.classList.add('btn-hide')
				} else {
					delbtn.classList.remove('btn-hide')
				}
			}
		}
		//Пееремещает кнопки удлаления
		mouseOver() {
			let eventTarget = event.target
			rowIndex = eventTarget.parentNode.sectionRowIndex
			cellIndex = eventTarget.cellIndex
			let position = eventTarget.getBoundingClientRect()
			delRow.style.top = position.top + pageYOffset + 'px'
			delCell.style.left = position.left + pageXOffset + 'px'
		}

		// добавляет к созданым ячейкам класс и событие, что бы кнопки удаления реагировали
		addNewCell(cell) {
			cell.classList.add('t-cell')
			cell.addEventListener('mouseover', this.mouseOver)
		}
	}
	
	let deladdel = new DelAddEl
	let button = new DisplayBtn

	for (let i = 0; i < td.length; i++) {
		td[i].addEventListener('mouseover', button.mouseOver)
	}

	table.addEventListener('mouseover', button.visibleBtn(true))
	table.addEventListener('mouseleave', timeOut)
	addRow.addEventListener('click', deladdel.addE)
	addCell.addEventListener('click', deladdel.addE)
	delRow.addEventListener('click', deladdel.deleteEl)
	delCell.addEventListener('click', deladdel.deleteEl)

}